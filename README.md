# Lookup Texture Example

The purpose of this repository is to show an OpenGL AMD bug when creating and uploading data as a texture to the GPU. 

## The problem
When uploading bit (mask) information into a floating point texture, the bitmask value is either rounded to zero, clamped or not written at all. 

## The case 
We want to encode additional material information into an own look up texture. The values that we want to encode are bitmasks or float values. For each material, its properties are stored in two 32-bit float pixels. 

In the example provided, we encode the bitmask values either via *reinterpret_cast* or with *glm::intBitsToFloat*. 

When we debug the provided example with renderdoc and view the uploaded texture as raw buffer via renderdoc, the bitmask values set on the CPU are clearly set to 0. 

![alt amd_rawbuffer_view](img/amd_rawbuffer_view.png)

Expected values for those 0 entries are: 
```
main:252 lutInfos.push_back(reinterpret_cast<float&>(3));
main:258 lutInfos.push_back(glm::intBitsToFloat(11));
main:268 lutInfos.push_back(reinterpret_cast<float&>(0x1001));
main:273 lutInfos.push_back(glm::intBitsToFloat(7));
```

When viewing the example with an installed nvidia card (gtx 1060, gtx 1080), we can not reproduce the issue: 
![alt nvidia_rawbuffer_view](img/nvidia_rawbuffer_view.png)

## The case with reading the bitmask
To avoid beeing deceived by the raw_buffer & texture visualization of renderdoc, we added a branch that tries to read the bitmask via *floatBitsToUint(sampledTexture.x)* and decide the color output dependent on the value. Which unfortunately supports the visualization of renderdoc and it seems to be a AMD driver issue.  

## Tests
We tested & reproduced the error with the following AMD cards: 
- AMD Radeon RX Vega 64: Radeon Software Adrenalin 2019 Edition 19.4.1 (25.20.15031.1000)
- AMD Radeon RX Vega 64: Radeon Software Adrenalin 2019 Edition 19.3 
- AMD Radeon RX Vega 64: Radeon Software Adrenalin 2019 Edition 19.1.1 (25.20.15011.1004)
- AMD Radeon RX Vega 64: Default windows driver(22.19.677.257)
- AMD Radeon RX 480: 25.20.15027.9004
