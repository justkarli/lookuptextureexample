#define GLEW_STATIC
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <iostream>
#include <vector>
#include <glm/glm.hpp>
#include <sstream>

void framebufferResizeCallback(GLFWwindow* window, int width, int height);
void processInput(GLFWwindow* window);
void glfwKeyCallback(GLFWwindow* window, int key, int scanCode, int action, int mods);
void glewErrorCallback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar *message, const GLvoid *userParam);

const char* vsSource = "#version 450 core		\n"
	"layout (location = 0) in vec3 pos;			\n"
	"layout (location = 1) in vec2 texcoord;	\n"
	"											\n"		
	"out vec2 uv;								\n"
	"void main()								\n"
	"{											\n"
	"	uv = texcoord;							\n"
	"	gl_Position = vec4(pos, 1.0f);			\n"
	"}											\n\0";

const char* fsIntegerSource = "#version 450 core	\n"
	"in vec2 uv;									\n"
	"out vec4 color;								\n"

	"uniform isampler2D lutTexture;					\n"
	"uniform int uReadBitFlag = 0;					\n"
	
	"void main()									\n"
	"{												\n"
	"	color = texture(lutTexture, uv);			\n"
	"	if(bool(uReadBitFlag))						\n"
	"	{											\n"
	"		int v = int(color.w);					\n"
	"		float fValue = intBitsToFloat(v);		\n"
	"		color = vec4(0, 0, 0, 1);				\n"
	"		if(fValue > 1.5f)						\n"
	"			color = vec4(1, 0, 0, 1);			\n"
	"	}											\n"
	"}												\n\0";

const char* fsfpSource = "#version 450 core			\n"
	"in vec2 uv;									\n"
	"out vec4 color;								\n"

	"uniform sampler2D lutTexture;					\n"
	"uniform int uReadBitFlag = 0;					\n"

	"void main()									\n"
	"{												\n"
	//"	color = vec4(1.0f, 0.5f, 0.2f, 1.0f);		\n"
	//"	color = vec4(uv, 0, 1);						\n"
	"	color = texture(lutTexture, uv);			\n"
	"	if(bool(uReadBitFlag))						\n"
	"	{											\n"
	"		uint bitFlag = floatBitsToUint(color.x);\n"
	"		color = vec4(0, 0, 0, 1);				\n"	
	"		if (bool(bitFlag & 1))					\n"
	"			color = vec4(1, 0, 0, 1);			\n"
	"	}											\n"
	"}												\n\0";

std::vector<int> createIntegerLookupData();
std::vector<float> createFloatingPointLookupData();

bool shaderReadBitflags = false;
bool useFloatingPointTexture = true;

int main()
{
	std::cout << "This program should demonstrate AMD's problem when uploading bitmask into fp textures" << std::endl;
	std::cout << "Press F6 to enable evaluating the decoded value. Expected outcome is that all pixels should be red. (Default: off)" << std::endl;
	std::cout << "Press F8 to switch the lut texture format float <-> int. (Default: float)" << std::endl;

	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GLFW_TRUE);

	GLFWwindow* window = glfwCreateWindow(800, 600, "Lookup Texture Example", nullptr, nullptr);

	if(window == nullptr)
	{
		std::cout << "Failed to create window" << std::endl;
		getchar();
		glfwTerminate();
		return -1;
	}

	glfwMakeContextCurrent(window);
	glfwSetFramebufferSizeCallback(window, framebufferResizeCallback);
	glfwSetKeyCallback(window, glfwKeyCallback);

	glewExperimental = true;
	if(glewInit() != GLEW_OK)
	{
		std::cout << "Failed to initialize glew" << std::endl;
		getchar();
		glfwTerminate();
		return -1;
	}

	glEnable(GL_DEBUG_OUTPUT);
	glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
	glDebugMessageCallback(glewErrorCallback, nullptr);
	glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, nullptr, GLFW_TRUE);

	int vertexShader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertexShader, 1, &vsSource, nullptr);
	glCompileShader(vertexShader);

	int success;
	char infoLog[512];
	glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);
	if(!success)
	{
		glGetShaderInfoLog(vertexShader, 512, nullptr, infoLog);
		std::cout << "Error compiling vertex shader\n" << infoLog << std::endl;
		getchar();
		glfwTerminate();
		return -1;
	}

	int fpfragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fpfragmentShader, 1, &fsfpSource, nullptr);
	glCompileShader(fpfragmentShader);

	glGetShaderiv(fpfragmentShader, GL_COMPILE_STATUS, &success);
	if(!success)
	{
		glGetShaderInfoLog(fpfragmentShader, 512, nullptr, infoLog);
		std::cout << "Error compiling fragment shader\n" << infoLog << std::endl;
		getchar();
		glfwTerminate();
		return -1;
	}

	int integerfragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(integerfragmentShader, 1, &fsIntegerSource, nullptr);
	glCompileShader(integerfragmentShader);

	glGetShaderiv(integerfragmentShader, GL_COMPILE_STATUS, &success);
	if (!success)
	{
		glGetShaderInfoLog(integerfragmentShader, 512, nullptr, infoLog);
		std::cout << "Error compiling fragment shader\n" << infoLog << std::endl;
		getchar();
		glfwTerminate();
		return -1;
	}

	int fpShaderProgram = glCreateProgram();
	glAttachShader(fpShaderProgram, vertexShader);
	glAttachShader(fpShaderProgram, fpfragmentShader);
	glLinkProgram(fpShaderProgram);

	glGetProgramiv(fpShaderProgram, GL_LINK_STATUS, &success);
	if(!success)
	{
		glGetProgramInfoLog(fpShaderProgram, 512, nullptr, infoLog);
		std::cout << "Error compiling program\n" << infoLog << std::endl;
		getchar();
		glfwTerminate();
		return -1;
	}

	int intShaderProgram = glCreateProgram();
	glAttachShader(intShaderProgram, vertexShader);
	glAttachShader(intShaderProgram, integerfragmentShader);
	glLinkProgram(intShaderProgram);

	glGetProgramiv(intShaderProgram, GL_LINK_STATUS, &success);
	if (!success)
	{
		glGetProgramInfoLog(intShaderProgram, 512, nullptr, infoLog);
		std::cout << "Error compiling program\n" << infoLog << std::endl;
		getchar();
		glfwTerminate();
		return -1;
	}

	glDeleteShader(vertexShader);
	glDeleteShader(fpfragmentShader);
	glDeleteShader(integerfragmentShader);

	unsigned int fpshaderUniformLocation = glGetUniformLocation(fpShaderProgram, "uReadBitFlag");
	unsigned int intshaderUniformLocation = glGetUniformLocation(intShaderProgram, "uReadBitFlag");

	float vertices[] = 
	{
		// position(vec3), uv(vec2)
		0.5f, 0.5f, 0.0f,	1.0f, 1.0f, 
		0.5f,-0.5f, 0.0f,	1.0f, 0.0f, 
		-0.5f, -0.5f, 0.0f, 0.0f, 0.0f, 
		-0.5f, 0.5f, 0.0f,	0.0f, 1.0f
	};

	unsigned int indices[] = 
	{
		0, 1, 3,
		1, 2, 3
	};

	unsigned int vao, vbo, ib;
	glGenVertexArrays(1, &vao);
	glGenBuffers(1, &vbo);
	glGenBuffers(1, &ib);

	glBindVertexArray(vao);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ib);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);

	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));
	glEnableVertexAttribArray(1);
	glBindVertexArray(0);

	std::vector<int> intlutInfos = createIntegerLookupData();
	std::vector<float> floatLutInfos = createFloatingPointLookupData();

	int width = intlutInfos.size() / 2 / 4; // # 2 float values per materials  (#size / #materials / #sizeof float )
	int height = 2;	// # materials

	unsigned int floattextureLutId;
	glGenTextures(1, &floattextureLutId);
	glBindTexture(GL_TEXTURE_2D, floattextureLutId);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, width, height, 0, GL_RGBA, GL_FLOAT, floatLutInfos.data());

	unsigned int inttextureLutId;
	glGenTextures(1, &inttextureLutId);
	glBindTexture(GL_TEXTURE_2D, inttextureLutId);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32I, width, height, 0, GL_RGBA_INTEGER, GL_INT, intlutInfos.data());

	while(!glfwWindowShouldClose(window))
	{
		processInput(window);

		glClearColor(0.2f, 0.2f, 0.2f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT);

		glBindVertexArray(vao);

		if(useFloatingPointTexture)
		{
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, floattextureLutId);

			glUseProgram(fpShaderProgram);
			glUniform1i(fpshaderUniformLocation, shaderReadBitflags ? 1 : 0);
		}
		else
		{
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, inttextureLutId);

			glUseProgram(intShaderProgram);
			glUniform1i(intshaderUniformLocation, shaderReadBitflags ? 1 : 0);
		}

		glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	glDeleteTextures(1, &floattextureLutId);
	glDeleteTextures(1, &inttextureLutId);

	glDeleteProgram(fpShaderProgram);
	glDeleteProgram(intShaderProgram);

	glDeleteVertexArrays(1, &vao);
	glDeleteBuffers(1, &vbo);
	glDeleteBuffers(1, &ib);

	glfwTerminate();
	return 0;
}

void framebufferResizeCallback(GLFWwindow* window, int width, int height)
{
	glViewport(0, 0, width, height);
}

void processInput(GLFWwindow* window)
{
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(window, true);
}

void glfwKeyCallback(GLFWwindow* window, int key, int scanCode, int action, int mods)
{
	if(key == GLFW_KEY_F6 && action == GLFW_PRESS)
	{
		shaderReadBitflags ^= 1;
		std::cout << "Switching shader uniform (Reading from bit flags): " << shaderReadBitflags << std::endl;
	}

	if(key == GLFW_KEY_F8 && action == GLFW_PRESS)
	{
		useFloatingPointTexture ^= 1;
		std::cout << "Switching lut format float(1) <-> int(0): " << useFloatingPointTexture << std::endl;
	}
}

/**
 * Generating look up table data. 
 * Material information is stored in two float(32-bit) RGBA pixels. 
 */
std::vector<int> createIntegerLookupData()
{
	std::vector<int> lutInfos;
	constexpr int FILLER = 1;

	constexpr bool MatOneFeatureOne = true;
	constexpr bool MatOneFeatureTwo = true;
	constexpr bool MatOneFeatureThree = false;
	constexpr bool MatOneFeatureFour = false;

	unsigned int materialOneFeatureBitFlags = 0;
	materialOneFeatureBitFlags |= MatOneFeatureOne << 0;
	materialOneFeatureBitFlags |= MatOneFeatureTwo << 1;
	materialOneFeatureBitFlags |= MatOneFeatureThree << 2;
	materialOneFeatureBitFlags |= MatOneFeatureFour << 3;

	// first pixel for material#1
	lutInfos.push_back(materialOneFeatureBitFlags);	// decimal: 3
	lutInfos.push_back(FILLER);
	lutInfos.push_back(FILLER);
	lutInfos.push_back(glm::floatBitsToInt(1.6f));

	// second pixel for material#1 
	lutInfos.push_back(11);
	// just some random data representing some material properties for material#1
	lutInfos.push_back(glm::floatBitsToInt(0.25f));
	lutInfos.push_back(glm::floatBitsToInt(0.8f));
	lutInfos.push_back(glm::floatBitsToInt(1.8f));

	// material#2
	// replacing features by a bitmask
	unsigned int materialTwoFeatureBitFlags = 0b1001;	// decimal: 9

	lutInfos.push_back(materialTwoFeatureBitFlags);
	lutInfos.push_back(FILLER);
	lutInfos.push_back(FILLER);
	lutInfos.push_back(glm::floatBitsToInt(5.0f));

	lutInfos.push_back(7);
	// just some random data representing some material properties for material#1
	lutInfos.push_back(glm::floatBitsToInt(0.5f));
	lutInfos.push_back(glm::floatBitsToInt(0.2f));
	lutInfos.push_back(glm::floatBitsToInt(3.8f));

	return lutInfos;
}

std::vector<float> createFloatingPointLookupData()
{
	std::vector<float> lutInfos;
	constexpr float FILLER = 1.0f;

	constexpr bool MatOneFeatureOne = true;
	constexpr bool MatOneFeatureTwo = true;
	constexpr bool MatOneFeatureThree = false;
	constexpr bool MatOneFeatureFour = false;

	unsigned int materialOneFeatureBitFlags = 0;
	materialOneFeatureBitFlags |= MatOneFeatureOne << 0;
	materialOneFeatureBitFlags |= MatOneFeatureTwo << 1;
	materialOneFeatureBitFlags |= MatOneFeatureThree << 2;
	materialOneFeatureBitFlags |= MatOneFeatureFour << 3;

	// first pixel for material#1
	lutInfos.push_back(glm::intBitsToFloat(materialOneFeatureBitFlags));	// decimal: 3
	lutInfos.push_back(FILLER);
	lutInfos.push_back(FILLER);
	lutInfos.push_back(1.6f);

	// second pixel for material#1 
	lutInfos.push_back(glm::intBitsToFloat(11));
	// just some random data representing some material properties for material#1
	lutInfos.push_back(0.25f);
	lutInfos.push_back(0.8f);
	lutInfos.push_back(1.8f);

	// material#2
	// replacing features by a bitmask
	unsigned int materialTwoFeatureBitFlags = 0b1001;	// decimal: 9

	lutInfos.push_back(glm::intBitsToFloat(materialTwoFeatureBitFlags));
	lutInfos.push_back(FILLER);
	lutInfos.push_back(FILLER);
	lutInfos.push_back(5.0f);

	lutInfos.push_back(glm::intBitsToFloat(7));
	// just some random data representing some material properties for material#1
	lutInfos.push_back(0.5f);
	lutInfos.push_back(0.2f);
	lutInfos.push_back(3.8f);

	return lutInfos;
}

void glewErrorCallback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar *message, const GLvoid *userParam)
{
	// ignore non-significant error/warning codes
	if (id == 131169 || id == 131185 || id == 131218 || id == 131204)
		return;

	const auto startsWith = [](const char *str, const char* pre)
	{
		const auto lenpre = strlen(pre);
		const auto lenstr = strlen(str);
		return lenstr < lenpre ? false : strncmp(pre, str, lenpre) == 0;
	};

	// skip shader recompile performance warning on Intel chips
	// TODO: what does this message mean ... there is no information about this warning available online
	if (source == GL_DEBUG_SOURCE_API && type == GL_DEBUG_TYPE_PERFORMANCE && severity == GL_DEBUG_SEVERITY_MEDIUM && startsWith(message, "API_ID_RECOMPILE_FRAGMENT_SHADER"))
		return;

	std::stringstream msg;

	msg << "GL Error Callback: ID=" << id << ": " << message;

	int errorId = id;
	int errorSource = 0;
	int errorType = 0;
	int errorSeverity = 0;

	msg << ", ";
	switch (source)
	{
	case GL_DEBUG_SOURCE_API:             msg << "Source: API"; errorSource = 1; break;
	case GL_DEBUG_SOURCE_WINDOW_SYSTEM:   msg << "Source: Window System"; errorSource = 2; break;
	case GL_DEBUG_SOURCE_SHADER_COMPILER: msg << "Source: Shader Compiler"; errorSource = 3; break;
	case GL_DEBUG_SOURCE_THIRD_PARTY:     msg << "Source: Third Party"; errorSource = 4; break;
	case GL_DEBUG_SOURCE_APPLICATION:     msg << "Source: Application"; errorSource = 5; break;
	case GL_DEBUG_SOURCE_OTHER:           msg << "Source: Other"; errorSource = 6; break;
	}

	msg << ", ";
	switch (type)
	{
	case GL_DEBUG_TYPE_ERROR:				msg << "Type: Error"; errorType = 1; break;
	case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR: msg << "Type: Deprecated Behaviour"; errorType = 2; break;
	case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:  msg << "Type: Undefined Behaviour"; errorType = 3; break;
	case GL_DEBUG_TYPE_PORTABILITY:         msg << "Type: Portability"; errorType = 4; break;
	case GL_DEBUG_TYPE_PERFORMANCE:         msg << "Type: Performance"; errorType = 5; break;
	case GL_DEBUG_TYPE_MARKER:              msg << "Type: Marker"; errorType = 6; return;			// ignore marker, push group & pop group messages
	case GL_DEBUG_TYPE_PUSH_GROUP:          msg << "Type: Push Group"; errorType = 7; return;		// are used to mark specific stages of the rendering (eg. renderdoc)
	case GL_DEBUG_TYPE_POP_GROUP:           msg << "Type: Pop Group"; errorType = 8; return;		// but they aren't actually error messages
	case GL_DEBUG_TYPE_OTHER:				msg << "Type: Other"; errorType = 9; break;
	}

	msg << ", ";
	switch (severity)
	{
	case GL_DEBUG_SEVERITY_HIGH:         msg << "Severity: high"; errorSeverity = 4; break;
	case GL_DEBUG_SEVERITY_MEDIUM:       msg << "Severity: medium"; errorSeverity = 3; break;
	case GL_DEBUG_SEVERITY_LOW:          msg << "Severity: low"; errorSeverity = 2; break;
	case GL_DEBUG_SEVERITY_NOTIFICATION: msg << "Severity: notification"; errorSeverity = 1; break;
	}

	// todo find out how "severe" this really is
	bool logNotifications = true;
	if (severity == GL_DEBUG_SEVERITY_NOTIFICATION && !logNotifications)
		return;

	std::cout << msg.str() << std::endl;
}